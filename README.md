*** Accelerator ***
- Introducción: 
Accelerator es una calculadora de velocidades tangenciales, específicamente en engranajes. Esta, además, es capaz de entregar el número de vueltas que da cada engranaje.
Para esta última entrega, daremos a conocer y explicaremos las nuevas funciones de nuestra calculadora, la cual empezó como un código ejecutable en terminal, y ahora incluso tiene su propia aplicación.
Como se mencionó anteriormente, dentro de este avance podremos ver la aplicación de Accelerator, además de sus nuevas mejoras en código, visualización y nuestras últimas actualizaciones en GitLab.

<< Objetivos >>
* Facilitación del cálculo de velocidades tangenciales en problemas de engranajes.
    - Especificos:
        - Creación de una aplicación que modele el resultado.
        - Creación de gráficos que muestren de mejor manera el resultado.
        - Implementación de cálculos.
* Lograr un mejor entendimiento por parte del usuario.
    - Especificos:
        - Reducción de material ineccesario en nuestro trabajo, ya sea en código, archivos, etc.
        - Orden en presentaciones de nuestro trabajo.
        - Orden por parte del grupo, para llevar de mejor manera las ideas y el orden de GitLab.

<< Avances >>
Para nuestro último avance, implementamos la calculadora en una aplicación. Esta, dentro de una pestaña emergente producida por QT, pregunta por el número de engranajes para posteriormente preguntar por los datos de los mismos (velocidad, radio y el tiempo). Finalmente, entrega el resultado de la velocidad tangencial, y el gráfico correspondiente.
Con respecto a los otros avances, podemos encontrar:

1. Mejora en los códigos fuentes, especialmente en el código referente a gráficos y la ventana emergente.
2. Visualización cada vez más estilizada y definida con respecto a sus versiones primitivas.
3. Mejora en el orden de GitLab, además de las últimas actualizaciones ya propuestas en su sistema.
4. Últimas propuestas hechas.

- Desarrollo:
Para nuestro trabajo, escogimos el uso de una calculadora que el usuario pueda utilizar de forma libre. En esta última entrega, al compilar el código, este le preguntara al usuario con cuantos engranajes quiere trabajar. Esto, para despues poder calcular la velocidad tangencial con los datos que la persona utilice. Escogimos esta manera, ya que nos parece importante que la persona pueda manejar libremente los datos para poder trabajar, sin tener la necesidad de cambiar el código o alguna otra cosa que pertenezca netamente a la matriz de nuestro trabajo. Ademas el método de ventanas emergentes y el gráfico nos parecen una buena manera de dar a entender al usuario de  mejro manera el tema, con la facilidad de ir cerrando cada ventana si es que ya no la necesita.

<< Requisitos de compilación >>
Con respecto a los requisitos, siguen siendo los mismos que la presentación anterior:

- El código ha sido compilado exitosamente en los sistemas operativos de Windows y Linux.
- Para poder compilar el código, se necesita tener descargado QT, ya que se necesita para poder obtener resultados óptimos como lo es la aplicación. Además de esto, se necesitan las otras funciones de QT como "QTCharts" y "QTCreator".
- Para poder utilizar el programa, deben intalar los archivos propuestos en la carpeta "src", para posteriormente compilar todo en la app de QT.

- Conclusión: 
Para concluir con nuestra presentación, y además nuestro trabajo, destacamos cada una de las cosas aprendidas por el mismo. Gracias a lo hecho dentro Accelerator, hemos aprendido a poder programar de una manera más completa y didáctica, en la que no solo esperamos un resultado simple, sino resultados gráficos e incluso las opciones de crear aplicaciones a base de esto. Además, cabe nombrar el desempeño usado para el trabajo, el trabajo en equipo y el planteamiento y solución a problemas dentro de nuestros avances. Si bien Accelerator es evaluado hasta acá, nos gustaría seguir investigando mucho más sobre una programación más detallada y estilizada para la aplicación de la calculadora. 

<< Referencias >>
Para las referencias, nos apoyamos principalmente en las clases impartidas en cátedra y las publicadas en AULA. Ademas de esto, para los teoremas y cálculos físicos utilizamos los siguientes libros:

* Giancoli, D. (s.f.). Fisica: Principios con Aplicaciones (Sexta ed.).

* Resnick, R., Halliday, D., & Krane, K. (1993). Fisica (Cuarta ed., Vol. I).

* Sears, Z., & Young, F. (s.f.). Fisica Universitaria (Decimo Primera ed., Vol. I Mecanica).

* Serway, J. (s.f.). Fisica Tomo I (Cuarta ed., Vol. I).

* Tipler, & Mosca. (2003). Fisica para la Ciencia y la Tecnologia (Quinta ed., Vol. I).

* Tipler, P. (2001). Fisica para la Ciencia y la Tecnologia (Cuarta ed., Vol. I).

* Tippens, P. (s.f.). Fisica: Conceptos y Aplicaciones (Sexta ed.).

Integrantes:
- Nicolás Alvarez (202230529-0)
- David Retuerto (202230568-1)
- Fernando Zamora (202230541-k)

Asignatura: Seminario de Programación
Profesor: Nicolás Galvez

Fecha: 07/12/2022
Fecha entrega: 10/12/2022
