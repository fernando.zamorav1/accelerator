#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_actionSalir_triggered();

    void on_actionreiniciar_triggered();

    void on_boton1_clicked();

    void on_boton2_clicked();

    void on_calcular_clicked();

    void on_pegado_clicked();

    void on_unosobreotro_clicked();

    void on_cuerda_clicked();

    void on_calcularP_clicked();

    void on_calcularC_clicked();

    void on_calcularUSO_clicked();

    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_3_clicked();

    void on_pushButton_4_clicked();

    void on_pushButton_5_clicked();

    void on_pushButton_6_clicked();

    void on_pushButton_7_clicked();

public slots:
    void mostrarpag1();
    void mostrarpag2();
    void mostrarpegados();
    void mostrarcuerda();
    void mostrarUSO();
    void ocultarpag1();
    void ocultarpag2();
    void ocultardoseng();


signals:
    void apretemostrar1();
    void apretemostrar2();
    void apretepegados();
    void apretecuerda();
    void apreteUSO();

private:
    Ui::MainWindow *ui;
};
#endif // MAINWINDOW_H
