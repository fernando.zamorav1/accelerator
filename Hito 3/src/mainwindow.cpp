#include "main.cpp"
#include "ui_mainwindow.h"
#include "FuncionesEng.h"
#include <QtWidgets/QApplication>
#include <QtWidgets/QMainWindow>
#include <QtCharts/QChartView>
#include <QtCharts/QLineSeries>

QT_CHARTS_USE_NAMESPACE


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    connect(this, SIGNAL(apretemostrar1()), this, SLOT(mostrarpag1()));
    connect(this, SIGNAL(apretemostrar1()), this, SLOT(ocultarpag1()));
    connect(this, SIGNAL(apretemostrar2()), this, SLOT(mostrarpag2()));
    connect(this, SIGNAL(apretemostrar2()), this, SLOT(ocultarpag2()));
    connect(this, SIGNAL(apretepegados()), this, SLOT(ocultardoseng()));
    connect(this, SIGNAL(apretepegados()), this, SLOT(mostrarpegados()));
    connect(this, SIGNAL(apreteUSO()), this, SLOT(ocultardoseng()));
    connect(this, SIGNAL(apreteUSO()), this, SLOT(mostrarUSO()));
    connect(this, SIGNAL(apretecuerda()), this, SLOT(ocultardoseng()));
    connect(this, SIGNAL(apretecuerda()), this, SLOT(mostrarcuerda()));
    ui->uneng->setVisible(false);
    ui->doseng->setVisible(false);
    ui->pegadosmenu->setVisible(false);
    ui->resultadosP->setVisible(false);
    ui->cuerdamenu->setVisible(false);
    ui->resultadosC->setVisible(false);
    ui->USOmenu->setVisible(false);
    ui->resultadosUSO->setVisible(false);
}

MainWindow::~MainWindow()
{
    delete ui;
}

//Opcion para salir del programa(arriba a la izquierda)
void MainWindow::on_actionSalir_triggered()
{
    MainWindow::close();
}
//Opcion para reiniciar el programa(arriba a la izquierda)
void MainWindow::on_actionreiniciar_triggered()
{
    ui->uneng->setVisible(false);
    ui->doseng->setVisible(false);
    ui->pegadosmenu->setVisible(false);
    ui->resultadosP->setVisible(false);
    ui->cuerdamenu->setVisible(false);
    ui->resultadosC->setVisible(false);
    ui->USOmenu->setVisible(false);
    ui->resultadosUSO->setVisible(false);
    ui->inicio->setVisible(true);
}
//Opcion un engranaje
void MainWindow::mostrarpag1(){
    ui->uneng->setVisible(true);
}
void MainWindow::ocultarpag1(){
    ui->inicio->setVisible(false);
}
void MainWindow::on_boton1_clicked()
{
  emit apretemostrar1();
}

void MainWindow::on_calcular_clicked(){
    QString tiemposeg=ui->TS->text();
    QString radiocm=ui->RM->text();
    QString vams=ui->VA->text();
    float tiempos=tiemposeg.toFloat();
    float radioc=radiocm.toFloat();
    float vam=vams.toFloat();
    float resul= un_eng(radioc,vam);
    float vuel=  n_vueltas(vam,tiempos);
    ui->resultado->setText("La velocidad del engranaje es de " + QString::number(resul)+"(m/s).");
    ui->vueltas->setText("Y el engranaje dio " + QString::number(vuel)+" vueltas.");

}

//Opcion 2 engranajes
void MainWindow::mostrarpag2(){
    ui->doseng->setVisible(true);
}
void MainWindow::ocultarpag2(){
    ui->inicio->setVisible(false);
}
void MainWindow::on_boton2_clicked()
{
  emit apretemostrar2();
}

void MainWindow::ocultardoseng(){
    ui->doseng->setVisible(false);
}

//Opcion pegados
void MainWindow::on_pegado_clicked()
{
    emit apretepegados();
}
void MainWindow::mostrarpegados(){
    ui->pegadosmenu->setVisible(true);
}

void MainWindow::on_calcularP_clicked(){
    QString tiemposeg1=ui->TS1P->text();
    QString radiocm1=ui->RM1P->text();
    QString vams1=ui->VA1P->text();
    float tiempos1=tiemposeg1.toFloat();
    float radioc1=radiocm1.toFloat();
    float vam1=vams1.toFloat();
    float resul1= un_eng(radioc1,vam1);
    float vuel1=  n_vueltas(vam1,tiempos1);
    QString tiemposeg2=ui->TS2P->text();
    QString radiocm2=ui->RM2P->text();
    QString vams2=ui->VA2P->text();
    float tiempos2=tiemposeg2.toFloat();
    float radioc2=radiocm2.toFloat();
    float vam2=vams2.toFloat();
    float resul2= un_eng(radioc2,vam2);
    float vuel2=  n_vueltas(vam2,tiempos2);
    ui->pegadosmenu->setVisible(false);
    ui->resultadosP->setVisible(true);
    ui->resultadop->setText("La velocidad del primer engranaje es de " + QString::number(resul1)+"(m/s)\n"+"y la del segundo engranaje es de " + QString::number(resul2)+"(m/s).");
    ui->vueltasp->setText("El primer engranaje dio " + QString::number(vuel1)+" vueltas\n"+"y el segundo engranaje dio " + QString::number(vuel2)+" vueltas.");
}
//Opcion uno sobre otro
void MainWindow::on_unosobreotro_clicked()
{
    emit apreteUSO();
}
void MainWindow::mostrarUSO(){
    ui->USOmenu->setVisible(true);
}

void MainWindow::on_calcularUSO_clicked()
{
    QString tiemposeg1=ui->TS1U->text();
    QString radiocm1=ui->RM1U->text();
    QString vams1=ui->VA1U->text();
    float tiempos1=tiemposeg1.toFloat();
    float radioc1=radiocm1.toFloat();
    float vam1=vams1.toFloat();
    float resul1= un_eng(radioc1,vam1);
    float vuel1=  n_vueltas(vam1,tiempos1);
    QString tiemposeg2=ui->TS2U->text();
    QString radiocm2=ui->RM2U->text();
    QString vams2=ui->VA2U->text();
    float tiempos2=tiemposeg2.toFloat();
    float radioc2=radiocm2.toFloat();
    float vam2=vams2.toFloat();
    float resul2= un_eng(radioc2,vam2);
    float vuel2=  n_vueltas(vam2,tiempos2);
    ui->USOmenu->setVisible(false);
    ui->resultadosUSO->setVisible(true);
    ui->resultadoUSO->setText("La velocidad del primer engranaje es de " + QString::number(resul1)+"(m/s)\n"+"y la del segundo engranaje es de " + QString::number(resul2)+"(m/s).");
    ui->vueltasUSO->setText("El primer engranaje dio " + QString::number(vuel1)+" vueltas\n"+"y el segundo engranaje dio " + QString::number(vuel2)+" vueltas.");
}


//Opcion Cuerda
void MainWindow::on_cuerda_clicked()
{
    emit apretecuerda();
}
void MainWindow::mostrarcuerda(){
    ui->cuerdamenu->setVisible(true);
}

void MainWindow::on_calcularC_clicked()
{
    QString tiemposeg1=ui->TS1C->text();
    QString radiocm1=ui->RM1C->text();
    QString vams1=ui->VA1C->text();
    float tiempos1=tiemposeg1.toFloat();
    float radioc1=radiocm1.toFloat();
    float vam1=vams1.toFloat();
    float resul1= un_eng(radioc1,vam1);
    float vuel1=  n_vueltas(vam1,tiempos1);
    QString tiemposeg2=ui->TS2C->text();
    QString radiocm2=ui->RM2C->text();
    QString vams2=ui->VA2C->text();
    float tiempos2=tiemposeg2.toFloat();
    float radioc2=radiocm2.toFloat();
    float vam2=vams2.toFloat();
    float resul2= un_eng(radioc2,vam2);
    float vuel2=  n_vueltas(vam2,tiempos2);
    ui->cuerdamenu->setVisible(false);
    ui->resultadosC->setVisible(true);
    ui->resultadoC->setText("La velocidad del primer engranaje es de " + QString::number(resul1)+"(m/s)\n"+"y la del segundo engranaje es de " + QString::number(resul2)+"(m/s).");
    ui->vueltasC->setText("El primer engranaje dio " + QString::number(vuel1)+" vueltas\n"+"y el segundo engranaje dio " + QString::number(vuel2)+" vueltas.");
}

//grafico para un engranaje
void MainWindow::on_pushButton_clicked()
{
    QString tiemposeg=ui->TS->text();
    QString radiocm=ui->RM->text();
    QString vams=ui->VA->text();
    float tiempos=tiemposeg.toFloat();
    float radioc=radiocm.toFloat();
    float vam=vams.toFloat();
    float resul= un_eng(radioc,vam);

    QLineSeries *series = new QLineSeries();

    series->append(0,resul);
    *series << QPointF(0, resul) << QPointF(tiempos, resul);

    QChart *chart = new QChart();
    chart->legend()->hide();
    chart->addSeries(series);
    chart->createDefaultAxes();
    chart->setTitle("velocidad angular");

    QChartView *chartView = new QChartView(chart);
    chartView->setRenderHint(QPainter::Antialiasing);
    chartView->show();
}

//botones para graficar 2 engranajes unidos por una cuerda
void MainWindow::on_pushButton_2_clicked()
{
    QString tiemposeg1=ui->TS1C->text();
    QString radiocm1=ui->RM1C->text();
    QString vams1=ui->VA1C->text();
    float tiempos1=tiemposeg1.toFloat();
    float radioc1=radiocm1.toFloat();
    float vam1=vams1.toFloat();
    float resul1= un_eng(radioc1,vam1);
    QLineSeries *series = new QLineSeries();

    series->append(0,resul1);
    *series << QPointF(0, resul1) << QPointF(tiempos1, resul1);

    QChart *chart = new QChart();
    chart->legend()->hide();
    chart->addSeries(series);
    chart->createDefaultAxes();
    chart->setTitle("velocidad angular");

    QChartView *chartView = new QChartView(chart);
    chartView->setRenderHint(QPainter::Antialiasing);
    chartView->show();
}


void MainWindow::on_pushButton_3_clicked()
{
    QString tiemposeg2=ui->TS2C->text();
    QString radiocm2=ui->RM2C->text();
    QString vams2=ui->VA2C->text();
    float tiempos2=tiemposeg2.toFloat();
    float radioc2=radiocm2.toFloat();
    float vam2=vams2.toFloat();
    float resul2= un_eng(radioc2,vam2);
    QLineSeries *series = new QLineSeries();

    series->append(0,resul2);
    *series << QPointF(0, resul2) << QPointF(tiempos2, resul2);

    QChart *chart = new QChart();
    chart->legend()->hide();
    chart->addSeries(series);
    chart->createDefaultAxes();
    chart->setTitle("velocidad angular");

    QChartView *chartView = new QChartView(chart);
    chartView->setRenderHint(QPainter::Antialiasing);
    chartView->show();
}

//botones para graficar 2 engranajes pegados uno al lado del otro
void MainWindow::on_pushButton_4_clicked()
{
    QString tiemposeg1=ui->TS1P->text();
    QString radiocm1=ui->RM1P->text();
    QString vams1=ui->VA1P->text();
    float tiempos1=tiemposeg1.toFloat();
    float radioc1=radiocm1.toFloat();
    float vam1=vams1.toFloat();
    float resul1= un_eng(radioc1,vam1);
    QLineSeries *series = new QLineSeries();

    series->append(0,resul1);
    *series << QPointF(0, resul1) << QPointF(tiempos1, resul1);

    QChart *chart = new QChart();
    chart->legend()->hide();
    chart->addSeries(series);
    chart->createDefaultAxes();
    chart->setTitle("velocidad angular");

    QChartView *chartView = new QChartView(chart);
    chartView->setRenderHint(QPainter::Antialiasing);
    chartView->show();
}


void MainWindow::on_pushButton_5_clicked()
{
    QString tiemposeg2=ui->TS2P->text();
    QString radiocm2=ui->RM2P->text();
    QString vams2=ui->VA2P->text();
    float tiempos2=tiemposeg2.toFloat();
    float radioc2=radiocm2.toFloat();
    float vam2=vams2.toFloat();
    float resul2= un_eng(radioc2,vam2);
    QLineSeries *series = new QLineSeries();

    series->append(0,resul2);
    *series << QPointF(0, resul2) << QPointF(tiempos2, resul2);

    QChart *chart = new QChart();
    chart->legend()->hide();
    chart->addSeries(series);
    chart->createDefaultAxes();
    chart->setTitle("velocidad angular");

    QChartView *chartView = new QChartView(chart);
    chartView->setRenderHint(QPainter::Antialiasing);
    chartView->show();
}

//botones para graficar 2 engranajes pegado uno encima del otro
void MainWindow::on_pushButton_6_clicked()
{
    QString tiemposeg1=ui->TS1U->text();
    QString radiocm1=ui->RM1U->text();
    QString vams1=ui->VA1U->text();
    float tiempos1=tiemposeg1.toFloat();
    float radioc1=radiocm1.toFloat();
    float vam1=vams1.toFloat();
    float resul1= un_eng(radioc1,vam1);
    QLineSeries *series = new QLineSeries();

    series->append(0,resul1);
    *series << QPointF(0, resul1) << QPointF(tiempos1, resul1);

    QChart *chart = new QChart();
    chart->legend()->hide();
    chart->addSeries(series);
    chart->createDefaultAxes();
    chart->setTitle("velocidad angular");

    QChartView *chartView = new QChartView(chart);
    chartView->setRenderHint(QPainter::Antialiasing);
    chartView->show();
}


void MainWindow::on_pushButton_7_clicked()
{
    QString tiemposeg2=ui->TS2U->text();
    QString radiocm2=ui->RM2U->text();
    QString vams2=ui->VA2U->text();
    float tiempos2=tiemposeg2.toFloat();
    float radioc2=radiocm2.toFloat();
    float vam2=vams2.toFloat();
    float resul2= un_eng(radioc2,vam2);
    QLineSeries *series = new QLineSeries();

    series->append(0,resul2);
    *series << QPointF(0, resul2) << QPointF(tiempos2, resul2);

    QChart *chart = new QChart();
    chart->legend()->hide();
    chart->addSeries(series);
    chart->createDefaultAxes();
    chart->setTitle("velocidad angular");

    QChartView *chartView = new QChartView(chart);
    chartView->setRenderHint(QPainter::Antialiasing);
    chartView->show();
}

