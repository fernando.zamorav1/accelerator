#ifndef FUNCIONES_H
#define FUNCIONES_H

#include <iostream>
#include <cmath>
#include <string>
using namespace std;

float radio;
float tiempo;
float va;
bool ciclo_main = true;
float un_eng(float radio, float velocidad_ang){
    float vt;
    vt = radio * velocidad_ang;
    return vt;
}
float n_vueltas(float velocidad_ang, int tiempo){
    float vuelta, pi;
    pi = 3.1415;
    vuelta = ((velocidad_ang * tiempo)/(pi * 2));
    return vuelta;
}
void engranaje_menu(int opcion, int num_engranaje, bool &ciclo_main){
    cout <<"Son 1 o 2 engranajes?" << endl;
    cout << "1. 1 " << endl;
    cout << "2. 2" << endl;
    cout << "\n3. Para Salir" << endl;
    cin >> opcion;
        if (opcion==1){
            float va, tie, r;
            cout << "Indique el radio en metros: ";
            cin >> r;
            cout << "Ingrese la velocidad angular en [m/s^2]: ";
            cin >> va;
            cout << "Indique el tiempo: ";
            cin >> tie;
            cout << "La velocidad tangencial del engranaje es: " << un_eng(r,va) << "[m/s^2]." << endl;
            cout << "El engranaje relizo " << n_vueltas(va,tie) << " vueltas." << endl;
        }
        if(opcion==2){
            int t;
            float r1,r2;
            cout << "Indique el radio del primer engranaje: ";
            cin >> r1;
            cout << "Indique el radio del segundo engranaje: ";
            cin >> r2;
            cout << "Indique el tipo de union entre los engranajes, ya sea pegados entre si (1), unidos por una cuerda (2) o uno sobre otro (3): ";
            cin >> t;

            if (t==1){
                float vac1,vac2,tie;
                cout << "Indique la velocidad angular del primer circulo: ";
                cin >> vac1;
                cout << "Indique la velocidad angular del segundo circulo: ";
                cin >> vac2;
                cout << "Indique el tiempo: ";
                cin >> tie;
                cout << "La velocidad tangencial del primer circulo es: " << un_eng(r1,vac1) << ", y del segundo es: " << un_eng(r2,vac2) << "." << endl;
                cout << " El primer circulo relizo " << n_vueltas(vac1,tie) << " vueltas, y el segundo circulo realizo " << n_vueltas(vac2,tie) << " vueltas." << endl;

            }if (t==2){
                float vac1,vac2,tie;
                cout << "Indique la velocidad angular del primer circulo: ";
                cin >> vac1;
                cout << "Indique la velocidad angular del segundo circulo: ";
                cin >> vac2;
                cout << "Indique el tiempo: ";
                cin >> tie;
                cout << "La velocidad tangencial del primer circulo es: " << un_eng(r1,vac1) << ", y del segundo es: " << un_eng(r2,vac2) << "." << endl;
                cout << " El primer circulo relizo " << n_vueltas(vac1,tie) << " vueltas, y el segundo circulo realizo " << n_vueltas(vac2,tie) << " vueltas." << endl;

            }if (t==3){
                float vac,tie;
                cout << "Indique la velocidad angular: ";
                cin >> vac;
                cout << "Indique el tiempo: ";
                cin >> tie;
                cout << "La velocidad tangencial del primer circulo es: " << un_eng(r1,vac) << ", y del segundo es: " << un_eng(r2,vac) << "." << endl;
                cout << " El ambos circulos relizaron " << n_vueltas(vac,tie) << " vueltas." << endl;
            }
        }if(opcion==3){
            exit(1);
        }
    }

#endif // FUNCIONES_H
