#include "mainwindow.h"
#include "Funciones.h"

#include <QApplication>


int main(int argc, char *argv[])
{

    while(ciclo_main) {
           int num_engranaje;
           int opcion;
           bool ciclo_main;

           engranaje_menu(opcion ,num_engranaje, ciclo_main);

           cout << "\nDesea realizar otra operacion?" << endl<<" (1 = si, 2 = no (salir)" << endl;
           int temp_continue;
           cin >> temp_continue;
           if (temp_continue == 1) {
               ciclo_main == true;
           }else if (temp_continue == 2) {
               cout << "Gracias por usar el programa" << endl;
               ciclo_main == false;
               break;
           } else {
               cout << "Opcion no valida" << endl;
           }
}

    QApplication a(argc, argv);
    MainWindow w;

    // 1) Crear la serie a graficar
    QLineSeries * vt = new QLineSeries();
    float i;
    for(i=0.0; i<=tiempo; i+=1.0){
        vt->append(i,un_eng(radio, va));
    }
    vt->setName("Velocidad Tangencial");



    // 2)Crear el grafico
    QChart * le_grafico = new QChart();
    le_grafico->addSeries(vt);


    le_grafico->createDefaultAxes();
    le_grafico->axes(Qt::Horizontal).first()->setRange(0,20);
    le_grafico->axes(Qt::Vertical).first()->setRange(0,100);


    // 3) Crear la salida de visualizacion
    QChartView * salida = new QChartView(le_grafico);

    // 3+) Elegir donde mostrar la salida visual
    w.setCentralWidget(salida);


    w.show();
    return a.exec();
    return 0;
}

