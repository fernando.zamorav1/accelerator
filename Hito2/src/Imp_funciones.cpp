#include "Funciones.h"

float un_eng(float radio, float velocidad_ang){
    float vt;
    vt = radio * velocidad_ang;
    return vt;
}
float n_vueltas(float velocidad_ang, int tiempo){
    float vuelta, pi;
    pi = 3.1415;
    vuelta = ((velocidad_ang * tiempo)/(pi * 2));
    return vuelta;
}
